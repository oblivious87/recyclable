# [Recyclable](http://jessestark.com/recycle) - A Project by Jesse Stark

## What is it?
This is a new design proposal to help better educate consumers about how
they can save the environment.  We buy all sorts of foods that come in
different containers.  Some need to go into the trash (boo!) and other
things should be recycled.  Instead of guessing or just throwing
everything in the trash, users can quickly scan the new label and find
if the container is recyclable in their area!

## How'd you come up with this?
Simple, I finished up a carton of orange juice.  I had no idea if the
container was reclclable.  The manufacurer put a nice big logo which 
stated **Recyclable!**, but in small print under it, they said *In Some 
Areas* and then instructed me to go to some website to see if I could 
recycle it.

I'm a lazy guy, so I didn't check it out, but instead started working on
this project.  I want a quick way for consumers to learn the rules for
recycling in their area!

## Feature Overview

- None, I just started working on it!

## Future Features

- Product pages for all sorts of recyclable materials
- Redesigned reclcle symbols built into a QR code
- Editable database build by users
- Special product pages for *Green Products*

## How Will it Make Money?

- It won't... I'll probably use Google Adsense to try to make a couple
bucks.

## License

This is a side project of mine to help build my portfolio.  If you want
to refine the code or adapt it for some other good cause, go for it!